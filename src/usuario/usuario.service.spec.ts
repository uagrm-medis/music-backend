import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { MockType, repositoryMockFactory } from 'src/mock-factory';
import { Repository } from 'typeorm';
import { UsuarioEntity } from './entities/usuario.entity';
import { UsuarioController } from './usuario.controller';
import { UsuarioService } from './usuario.service';

const id = 1;
const usuario = "usuario1";
const email = 'name@email.com';
const rol = 'admin';
const premium = 0;

const usuario1 = { id, usuario, email, rol, premium };
const usuario2 = {
  id: 2,
  usuario: 'usuario2',
  email: 'name@email.com',
  rol: 'guest',
  premium: 1,
};
const usuarios = [usuario1, usuario2];

describe('UsuarioService', () => {
  let service: UsuarioService;
  let repositoryMock: MockType<Repository<UsuarioEntity>>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsuarioController],
      providers: [
        UsuarioService,
        {
          provide: getRepositoryToken(UsuarioEntity),
          useFactory: repositoryMockFactory,
        },
      ],
    }).compile();

    service = module.get<UsuarioService>(UsuarioService);
    repositoryMock = module.get(getRepositoryToken(UsuarioEntity));
  });

  it('debe estar definido', () => {
    expect(service).toBeDefined();
  });

  // it('debe insertar con éxito un nuevo Usuario', () => {
  //   repositoryMock.save.mockReturnValue(usuario1);
  //   const createDto = { usuario, email, rol, premium };
  //   expect(service.create(createDto)).resolves.toEqual(usuario1);
  //   expect(repositoryMock.save).toBeCalledTimes(1);
  //   expect(repositoryMock.save).toBeCalledWith({
  //     id, 
  //     usuario, 
  //     email, 
  //     rol, 
  //     premium
  //   });
  //   expect(repositoryMock.save).toBeCalledTimes(1);
  // });

  it('debe obtener una lista de Usuarios', async () => {
    repositoryMock.find.mockReturnValue(usuarios);
    expect(service.findAll()).resolves.toEqual(usuarios);
    expect(repositoryMock.find).toHaveBeenCalled();
  });

  it('debe devolver un Usuario con base a un id', async () => {
    repositoryMock.findOne.mockReturnValue(usuario1);
    expect(service.findOne(usuario1.id)).resolves.toEqual(usuario1);
    expect(repositoryMock.findOne).toHaveBeenCalled();
  });

  it('debe modificar con éxito un Usuarios', async () => {
    const updateAlbumDto = {
      usuario: 'usuario2update',
      email: 'name@email.com',
      rol: 'secretary',
      premium: 0,
    };
    const expectedResult = { id, ...updateAlbumDto };

    repositoryMock.findOne.mockReturnValue(usuario1);
    repositoryMock.save.mockReturnValue(expectedResult);

    const interpreteUpdated = await service.update(id, updateAlbumDto);
    expect(interpreteUpdated).toEqual(expectedResult);
    expect(repositoryMock.save).toBeCalledTimes(1);
  });

  it('debe eliminar con éxito un Usuario', async () => {
    repositoryMock.findOne.mockReturnValue(usuario1);
    repositoryMock.delete.mockReturnValue(true);
    expect(service.remove(id)).resolves.toEqual(true);
  });
});
