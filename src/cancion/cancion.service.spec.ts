import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { MockType, repositoryMockFactory } from 'src/mock-factory';
import { Repository } from 'typeorm';
import { CancionEntity } from './entities/cancion.entity';
import { CancionController } from './cancion.controller';
import { CancionService } from './cancion.service';

const id = 1;
const idAlbum = 1;
const idGenero = 1;
const nombre = 'cancion1';
const duracion = '03:01';
const tags = 'Cancion de los 80';
const url = 'https://cancion1.com/';

const cancion = { id, idAlbum, idGenero, nombre, duracion, tags, url };
const cancion2 = {
  id: 2,
  idAlbum: 1,
  idGenero: 1,
  nombre: 'cancion2',
  duracion: '03:01',
  tags: 'Cancion2 de los 80',
  url: 'https://cancion2.com/'
};
const albumes = [cancion, cancion2];

describe('CancionService', () => {
  let service: CancionService;
  let repositoryMock: MockType<Repository<CancionEntity>>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CancionController],
      providers: [
        CancionService,
        {
          provide: getRepositoryToken(CancionEntity),
          useFactory: repositoryMockFactory,
        },
      ],
    }).compile();

    service = module.get<CancionService>(CancionService);
    repositoryMock = module.get(getRepositoryToken(CancionEntity));
  });

  it('debe estar definido', () => {
    expect(service).toBeDefined();
  });

  it('debe insertar con éxito una nueva canción', () => {
    repositoryMock.save.mockReturnValue(cancion);
    const createDto = { id, idAlbum, idGenero, nombre, duracion, tags, url };
    expect(service.create(createDto)).resolves.toEqual(cancion);
    expect(repositoryMock.save).toBeCalledTimes(1);
    expect(repositoryMock.save).toBeCalledWith({
      idAlbum, 
      idGenero, 
      nombre, 
      duracion, 
      tags, 
      url
    });
    expect(repositoryMock.save).toBeCalledTimes(1);
  });

  it('debe obtener una lista de Canciones', async () => {
    repositoryMock.find.mockReturnValue(albumes);
    expect(service.findAll()).resolves.toEqual(albumes);
    expect(repositoryMock.find).toHaveBeenCalled();
  });

  it('debe devolver una Canción en base a un id', async () => {
    repositoryMock.findOne.mockReturnValue(cancion);
    expect(service.findOne(cancion.id)).resolves.toEqual(cancion);
    expect(repositoryMock.findOne).toHaveBeenCalled();
  });

  it('debe modificar con éxito un Canción', async () => {
    const updateCancionDto = {
      nombre: 'U-' + nombre,
      duracion: '4:01', 
      tags: 'pop', 
      url: 'https://cancionnueva.com/'
    };
    const expectedResult = { id, ...updateCancionDto };

    repositoryMock.findOne.mockReturnValue(cancion);
    repositoryMock.save.mockReturnValue(expectedResult);

    const cancionUpdated = await service.update(id, updateCancionDto);
    expect(cancionUpdated).toEqual(expectedResult);
    expect(repositoryMock.save).toBeCalledTimes(1);
  });

  it('debe eliminar con éxito una Canción', async () => {
    repositoryMock.findOne.mockReturnValue(cancion);
    repositoryMock.delete.mockReturnValue(true);
    expect(service.remove(id)).resolves.toEqual(true);
  });
});
