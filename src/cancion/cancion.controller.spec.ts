import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { repositoryMockFactory } from 'src/mock-factory';
import { CancionController } from './cancion.controller';
import { CancionService } from './cancion.service';
import { CancionEntity } from './entities/cancion.entity';

describe('CancionController', () => {
  let module: TestingModule;
  let controller: CancionController;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [CancionController],
      providers: [CancionService, { provide: getRepositoryToken(CancionEntity), useFactory: repositoryMockFactory }],
    }).compile();

    controller = module.get<CancionController>(CancionController);
  });

  afterAll(async () => {
    await module.close();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
