import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCancionDto } from './dto/create-cancion.dto';
import { UpdateCancionDto } from './dto/update-cancion.dto';
import { CancionEntity } from './entities/cancion.entity';

@Injectable()
export class CancionService {
  constructor(
    @InjectRepository(CancionEntity)
    private repository: Repository<CancionEntity>
  ){}
  
 async create(createCancionDto: CreateCancionDto) {
    const newCancion = await this.repository.save({
      idAlbum: createCancionDto.idAlbum,
      idGenero: createCancionDto.idGenero,
      nombre: createCancionDto.nombre,
      duracion: createCancionDto.duracion,
      tags: createCancionDto.tags,
      url: createCancionDto.url
    })
    return newCancion;
  }

  async findAll() {
    return await this.repository.find({ relations: { album: true, genero: true } });
  }

  async findOne(id: number) {
    const existe = await this.repository.findOne({
      where: { id },
      relations: { album: true, genero:true },
    });
    if (!existe) throw new NotFoundException(`La canción ${id} no existe`);
    return existe;
  }

  async update(id: number, updateCancionDto: UpdateCancionDto): Promise<CancionEntity> {
    const existe = await this.repository.findOne({ where: { id } });
    if (!existe) throw new NotFoundException(`La canción ${id} no existe`);
    const updateAlbum = Object.assign(existe, updateCancionDto);
    return await this.repository.save(updateAlbum);
  }

  async remove(id: number): Promise<boolean> {
    const existe = await this.repository.findOne({ where: { id } });
    if (!existe) throw new NotFoundException(`La canción ${id} no existe`);
    this.repository.delete(id);
    return true;  }
}
